package com.epam.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Getter
@Setter
@Table(name = "Tags")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long tagId;


    @Column(name = "name")
    private String name;


    @JsonIgnore
    @ManyToMany(mappedBy = "assignedTags")
    private Set<GiftCertificate> giftsSets = new HashSet<>();

}
