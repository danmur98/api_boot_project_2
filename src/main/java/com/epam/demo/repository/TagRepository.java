package com.epam.demo.repository;

import com.epam.demo.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;


public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> findAllByTagId(Long tagId);
    // List<Tag> findTagsByGiftCertificatesId(Long gifId);
}