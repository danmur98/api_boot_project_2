package com.epam.demo.repository;

import com.epam.demo.entity.GiftCertificate;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;


public interface GiftRepository extends JpaRepository<GiftCertificate, Long> {
    List<GiftCertificate> findAllByGiftId(Long giftId);
    // List<GiftCertificate> findGiftCertificatesByTagsId(Long tagId);
}
