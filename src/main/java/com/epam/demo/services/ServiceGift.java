package com.epam.demo.services;

import com.epam.demo.repository.GiftRepository;
import com.epam.demo.repository.TagRepository;
import com.epam.demo.entity.GiftCertificate;
import com.epam.demo.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ServiceGift {

    @Autowired
    private GiftRepository giftRepository;

    @Autowired
    private TagRepository tagRepository;



    public void saveGiftCertificate(GiftCertificate giftCertificate) {
        giftRepository.save(giftCertificate);
    }

    public List<GiftCertificate> getGiftCertificates(Long giftId) {
        if (null != giftId) {
            return giftRepository.findAllByGiftId(giftId);
        } else {
            return giftRepository.findAll();
        }
    }

    public void deleteGiftCertificate(Long giftId)  {
        giftRepository.deleteById(giftId);
    }

    public GiftCertificate assignTagToCertificate(Long giftId, Long tagId) {
        Set<Tag> tagSet = null;
        GiftCertificate giftCertificate = giftRepository.findById(giftId).orElse(null);
        Tag tag = tagRepository.findById(tagId).orElse(null);
        tagSet =  giftCertificate.getAssignedTags();
        tagSet.add(tag);
        giftCertificate.setAssignedTags(tagSet);
        return giftRepository.save(giftCertificate);
    }

    public GiftCertificate getCertificate (Long giftId) {
        return giftRepository.findById(giftId).get();
    }

    public GiftCertificate updateGiftCertificate(GiftCertificate giftCertificate) {
        return giftRepository.save(giftCertificate);
    }



}
