package com.epam.demo.services;

import com.epam.demo.repository.TagRepository;
import com.epam.demo.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceTag {


    @Autowired
    private TagRepository tagRepository;

    public ServiceTag(TagRepository tagRepository) {
    }


    public void saveTag(Tag tag) {
        tagRepository.save(tag);
    }

    public List<Tag> getTags(Long tagId) {
        if (null != tagId) {
            return tagRepository.findAllByTagId(tagId);
        } else {
            return tagRepository.findAll();
        }
    }

    public void deleteTag(Long tagId) {
        tagRepository.deleteById(tagId);
    }
}
