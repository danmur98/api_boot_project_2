package com.epam.demo.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;



@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.epam.demo.controller"))
                .paths(PathSelectors.regex("/gifts.*|/tags.*"))
                .build()
                .apiInfo(info());
    }

    private ApiInfo info(){

        return  new ApiInfo("gifts certificates api",
                "capstone project task for fast track epam",
                "0.0.1",
                "local only",
                new springfox.documentation.service.Contact("Daniel Anaya Murcia","http:/unapi.oi","fakemail@gmail.com"),
                "license",
                "http:/website.real",
                Collections.emptyList());

    }

}
