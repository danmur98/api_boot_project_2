package com.epam.demo.controller;


import com.epam.demo.services.ServiceGift;
import com.epam.demo.entity.GiftCertificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gifts")
public class GiftController {



    @Autowired
    private ServiceGift serviceGift;

    public GiftController(ServiceGift serviceGift) {
    }


    @PostMapping("/saveGift")
    public ResponseEntity<GiftCertificate> saveCertificate(@RequestBody GiftCertificate giftCertificate) {
        serviceGift.saveGiftCertificate(giftCertificate);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = {"/getGifts", "/{giftId}"})
    public List<GiftCertificate> getCertificate(@PathVariable(required = false) Long giftId) {
        return serviceGift.getGiftCertificates(giftId);
    }

    @DeleteMapping("deleteGifts/{giftId}")
    public ResponseEntity removeCertificate(@PathVariable Long giftId){
        serviceGift.deleteGiftCertificate(giftId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/{giftId}/tags/{tagId}")
    public GiftCertificate assignTagCertificate(
            @PathVariable Long giftId,
            @PathVariable Long tagId
    ){
        return serviceGift.assignTagToCertificate(giftId, tagId);
    }




}
