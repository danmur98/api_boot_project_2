package com.epam.demo.controller;

import com.epam.demo.services.ServiceTag;
import com.epam.demo.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tags")

public class TagController {


    @Autowired
    private ServiceTag serviceTag;


    @PostMapping("/saveTag")
    public ResponseEntity createTag(@RequestBody Tag tag) {
        serviceTag.saveTag(tag);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = {"/getTags", "/{tagId}"})
    public List<Tag> getTags(@PathVariable(required = false) Long tagId) {
        return serviceTag.getTags(tagId);
    }

    @DeleteMapping("/deleteTags/{tagId}")
    public ResponseEntity removeTag(@PathVariable Long tagId) {
        serviceTag.deleteTag(tagId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
