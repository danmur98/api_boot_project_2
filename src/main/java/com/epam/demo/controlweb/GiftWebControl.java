package com.epam.demo.controlweb;


import com.epam.demo.entity.GiftCertificate;
import com.epam.demo.entity.Tag;
import com.epam.demo.repository.GiftRepository;
import com.epam.demo.repository.TagRepository;
import com.epam.demo.services.ServiceGift;
import com.epam.demo.services.ServiceTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

@Controller
public class GiftWebControl {

    @Autowired
    GiftRepository giftRepository;

    @Autowired
    GiftRepository ServiceGift;

    @Autowired
    ServiceGift SerGift;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    ServiceTag serviceTag;

    @GetMapping({"/"})
    public String index() {
        return "index";
    }

    @GetMapping({"/gifts"})
    public String index(Model model) {
        model.addAttribute("gifts",ServiceGift.findAll());
        return "gifts";
    }

    @GetMapping ("/gifts/new")
    public String ShowGiftForm (Model model){
        GiftCertificate giftCertificate= new GiftCertificate();
        model.addAttribute("gift",giftCertificate);
        return "create_gift";
    }

    @PostMapping ("/gifts")
    public String CreateGifts (@ModelAttribute("gift") GiftCertificate giftCertificate){
        ServiceGift.save(giftCertificate);
        return "redirect:/gifts";
    }


    @GetMapping("/gifts/del/{giftId}")
    public String deleteGift (@PathVariable Long giftId){
     ServiceGift.deleteById(giftId);
     return "redirect:/gifts";

    }


    @GetMapping("/gifts/see/{giftId}")
    public String seeGift(@PathVariable Long giftId, Model model) {
        model.addAttribute("gift",SerGift.getCertificate(giftId));
        return "edit_gift"; // Redirige a la lista de Tags después de eliminar un Tag
    }

    @PostMapping ("/gifts/update/{giftId}")
    public String updateGifts (@PathVariable Long giftId ,@ModelAttribute ("gift") GiftCertificate giftCertificate, Model model){
        GiftCertificate existingift = SerGift.getCertificate(giftId);
        existingift.setGiftId(giftId);
        existingift.setName(giftCertificate.getName());
        existingift.setDescription(giftCertificate.getDescription());
        existingift.setPrice(giftCertificate.getPrice());
        existingift.setDuration(giftCertificate.getDuration());
        existingift.setCreateDate(giftCertificate.getCreateDate());
        existingift.setLastUpdateDate(giftCertificate.getLastUpdateDate());
        SerGift.updateGiftCertificate(existingift);
        return "redirect:/gifts";
    }






    @GetMapping("/tags")
    public String listTags(Model model) {
        List<Tag> tags = tagRepository.findAll();
        model.addAttribute("tags", tags);
        return "tags"; // Crea una página "list.html" para mostrar la lista de Tags
    }

    @GetMapping("/tags/new")
    public String showTagForm(Model model) {
        model.addAttribute("tag", new Tag());
        return "create_tag"; // Crea una página "create.html" para el formulario de creación de Tags
    }

    @PostMapping("/tags")
    public String createTag(@ModelAttribute("tag") Tag tag) {
        tagRepository.save(tag);
        return "redirect:/tags"; // Redirige a la lista de Tags después de crear uno nuevo
    }

    @GetMapping("/tags/del/{tagId}")
    public String deleteTag(@PathVariable Long tagId) {
        tagRepository.deleteById(tagId);
        return "redirect:/tags"; // Redirige a la lista de Tags después de eliminar un Tag
    }

    @GetMapping("/gifts/assign-tag/{giftId}")
    public String showAssignTagForm(@PathVariable Long giftId, Model model) {
        GiftCertificate giftCertificate = SerGift.getCertificate(giftId);
        List<Tag> availableTags = tagRepository.findAll();

        if (giftCertificate != null) {
            model.addAttribute("gift", giftCertificate);
            model.addAttribute("availableTags", availableTags);
            return "assign_tag"; // Crea una página "assign_tag.html" para la asignación de tags
        } else {
            // Manejar el error, el regalo no se encontró
            return "redirect:/gifts";
        }
    }

    @PostMapping("/gifts/assign-tag/{giftId}")
    public String assignTagToCertificate(@PathVariable Long giftId, @RequestParam Long tagId) {
        GiftCertificate giftCertificate = SerGift.getCertificate(giftId);
        Tag tag = tagRepository.findById(tagId).orElse(null);

        if (giftCertificate != null && tag != null) {
            Set<Tag> assignedTags = giftCertificate.getAssignedTags();
            assignedTags.add(tag);
            giftCertificate.setAssignedTags(assignedTags);
            giftRepository.save(giftCertificate);
        }

        return "redirect:/gifts";
    }






}




