package com.epam.demo;


import com.epam.demo.controller.GiftController;
import com.epam.demo.entity.GiftCertificate;
import com.epam.demo.entity.Tag;
import com.epam.demo.repository.GiftRepository;
import com.epam.demo.repository.TagRepository;
import com.epam.demo.services.ServiceGift;
import com.epam.demo.services.ServiceTag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;


@SpringBootTest
public class GiftServiceTests {

    @Mock
    private GiftRepository giftRepository;

    @Mock
    private TagRepository tagRepository;


    @InjectMocks
    private ServiceGift serviceGift;

    @InjectMocks
    private ServiceTag serviceTag;





    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);


    }


    @Test
    void getGiftCertificatesTest (){

        GiftCertificate esperado = new GiftCertificate();
        esperado.setGiftId(1L);
        esperado.setName("xbox one");
        esperado.setDescription("xbox one con dos controles más un juego de gears of war5");
        esperado.setPrice(BigDecimal.valueOf(1000));
        esperado.setCreateDate("2015-08-27");
        esperado.setLastUpdateDate("2015-08-27");
        esperado.setAssignedTags(new HashSet<>());


        var expected = Arrays.asList(esperado);
        Mockito.when(giftRepository.findAllByGiftId(1L)).thenReturn(expected);
        final List<GiftCertificate> resultado = serviceGift.getGiftCertificates(1L);

        Assertions.assertEquals(expected,resultado);



    }

    @Test
    void assignTagToCertificateTest() {
        // Crear un certificado de regalo simulado
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setGiftId(1L);
        giftCertificate.setName("xbox one");
        giftCertificate.setDescription("xbox one con dos controles más un juego de gears of war5");
        giftCertificate.setPrice(BigDecimal.valueOf(1000));
        giftCertificate.setCreateDate("2015-08-27");
        giftCertificate.setLastUpdateDate("2015-08-27");
        giftCertificate.setAssignedTags(new HashSet<>());

        // Crear un tag simulado
        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setName("Electronics");

        // Configurar el comportamiento esperado del repositorio para findById
        Mockito.when(giftRepository.findById(1L)).thenReturn(Optional.of(giftCertificate));
        Mockito.when(tagRepository.findById(1L)).thenReturn(Optional.of(tag));

        // Llamar al método assignTagToCertificate
        serviceGift.assignTagToCertificate(1L, 1L);

        // Verificar que el tag se haya asignado al certificado de regalo
        Assertions.assertEquals(1, giftCertificate.getAssignedTags().size());
        Assertions.assertEquals(tag, giftCertificate.getAssignedTags().iterator().next());
    }


    @Test
    void whenAddGiftCertificate_thenVerified() {
        GiftCertificate giftCertificate = mock(GiftCertificate.class);
        giftCertificate.setGiftId(1L);
        giftCertificate.setName("xbox one");
        giftCertificate.setDescription("xbox one con dos controles más un juego de gears of war5");
        giftCertificate.setPrice(BigDecimal.valueOf(1000));
        giftCertificate.setCreateDate("2015-08-27");
        giftCertificate.setLastUpdateDate("2015-08-27");
        giftCertificate.setAssignedTags(new HashSet<>());

        serviceGift.saveGiftCertificate(giftCertificate);
        verify(giftRepository, times(1)).save(giftCertificate);


    }


    @Test
    public void testDeleteDose() {
        GiftCertificate giftCertificate = mock(GiftCertificate.class);
        giftCertificate.setGiftId(2L);
        giftCertificate.setName("xbox one");
        giftCertificate.setDescription("xbox one con dos controles más un juego de gears of war5");
        giftCertificate.setPrice(BigDecimal.valueOf(1000));
        giftCertificate.setCreateDate("2015-08-27");
        giftCertificate.setLastUpdateDate("2015-08-27");
        giftCertificate.setAssignedTags(new HashSet<>());

        // perform the call
        serviceGift.deleteGiftCertificate(2L);

        // verify the mocks
        verify(giftRepository, times(1)).deleteById(eq(2L));
    }

}
