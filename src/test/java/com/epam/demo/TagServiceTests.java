package com.epam.demo;

import com.epam.demo.entity.GiftCertificate;
import com.epam.demo.entity.Tag;
import com.epam.demo.repository.TagRepository;
import com.epam.demo.services.ServiceGift;
import com.epam.demo.services.ServiceTag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest
public class TagServiceTests {


    @Mock
    private TagRepository tagRepository;

    @InjectMocks
    private ServiceTag serviceTag;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);


    }


    @Test
    void whenAddTag_thenVerified() {
       Tag tag = new Tag();
       tag.setTagId(1L);
       tag.setName("maria");
        serviceTag.saveTag(tag);
        verify(tagRepository, times(1)).save(tag);


    }


    @Test
    public void testDeleteDose() {
        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setName("pedro");
        serviceTag.deleteTag(1L);


        // verify the mocks
        verify(tagRepository, times(1)).deleteById(eq(1L));
    }


    @Test
    void getGiftCertificatesTest (){

        Tag tag = new Tag();
        tag.setTagId(2L);
        tag.setName("pedro");

        var expected = Arrays.asList(tag);
        Mockito.when(tagRepository.findAllByTagId(2L)).thenReturn(expected);
        final List<Tag> resultado = serviceTag.getTags(2L);

        Assertions.assertEquals(expected,resultado);





    }




}
